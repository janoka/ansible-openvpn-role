# Janoka.OpenVPN Role for Ansible

## More information

- [EASY-RSA Documentation](https://easy-rsa.readthedocs.io/)
- [How to setup a OpenVPN server on Ubuntu 20.04](https://linuxconfig.org/how-to-setup-a-openvpn-server-on-ubuntu-20-04)

## Generate Client Certs

```shell
# Generate clients.
./easyrsa --req-email=info@example.com build-client-full info@example.com
```

